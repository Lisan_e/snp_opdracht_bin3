#!/usr/bin/env python3

"""
Short script to access the import or lack there off while introducing an SNP into an alignment.
"""

__author__ = "Lisan Eisinga"
__version__ = 1.03

import sys
import argparse
import math

TABLE = {
    'ATA': 'I', 'ATC': 'I', 'ATT': 'I', 'ATG': 'M',
    'ACA': 'T', 'ACC': 'T', 'ACG': 'T', 'ACT': 'T',
    'AAC': 'N', 'AAT': 'N', 'AAA': 'K', 'AAG': 'K',
    'AGC': 'S', 'AGT': 'S', 'AGA': 'R', 'AGG': 'R',
    'CTA': 'L', 'CTC': 'L', 'CTG': 'L', 'CTT': 'L',
    'CCA': 'P', 'CCC': 'P', 'CCG': 'P', 'CCT': 'P',
    'CAC': 'H', 'CAT': 'H', 'CAA': 'Q', 'CAG': 'Q',
    'CGA': 'R', 'CGC': 'R', 'CGG': 'R', 'CGT': 'R',
    'GTA': 'V', 'GTC': 'V', 'GTG': 'V', 'GTT': 'V',
    'GCA': 'A', 'GCC': 'A', 'GCG': 'A', 'GCT': 'A',
    'GAC': 'D', 'GAT': 'D', 'GAA': 'E', 'GAG': 'E',
    'GGA': 'G', 'GGC': 'G', 'GGG': 'G', 'GGT': 'G',
    'TCA': 'S', 'TCC': 'S', 'TCG': 'S', 'TCT': 'S',
    'TTC': 'F', 'TTT': 'F', 'TTA': 'L', 'TTG': 'L',
    'TAC': 'Y', 'TAT': 'Y', 'TAA': '_', 'TAG': '_',
    'TGC': 'C', 'TGT': 'C', 'TGA': '_', 'TGG': 'W'}


def find_amino(pos, snp, sequence):
    """
    Finds the amino acids/triplets associated with the SNP.
    :param pos: Position of the mutation.
    :param snp: The new nucleotide.
    :param sequence: The used sequence
    :return:
    """
    sequence_file = open(sequence)
    sequence = ""
    for line in sequence_file:
        if not line.startswith(">"):
            sequence += line.strip()
    triplet = sequence[pos - (pos % 3):pos + (3 - (pos % 3))]
    triplet_new = list(triplet)
    triplet_new[(pos % 3)] = snp
    triplet_new = ''.join(triplet_new)

    aa_acid = TABLE[triplet]
    new_aa = TABLE[triplet_new]
    return aa_acid, new_aa


def mutation_effect(pos, new_amino_acid):
    """
    :param pos: Position
    :param new_amino_acid: The newly recorded amino acid.
    """
    proteins = []
    scores = {}
    sequences = msa_reader("MSA.clustal")

    # Puts all the proteins on location pos in a list
    for proteinsequence in sequences:
        proteins.append(sequences[proteinsequence][pos])

    # Calculates the score for each protein
    for protein in set(proteins):
        scores[protein] = proteins.count(protein) / len(proteins) * 10

    # Returns the score
    if new_amino_acid in scores:
        return round(scores[new_amino_acid], 2)
    else:
        return 0


def msa_reader(msa):
    """
    Reads in the MSA file.
    :param msa: The path to the MSA file.
    :return:
    """
    msa_file_opener = open(msa)
    score_seq = {}
    for line in msa_file_opener:
        if line.startswith("gi"):
            line = line.strip()
            line_seq = line[:30]
            if line_seq in score_seq:
                score_seq[line_seq] += line[36:]
            else:
                score_seq[line_seq] = line[36:]
    return score_seq


def determine_severity(score):
    """
    Gives feedback on mutation severity.
    :param score: gives the score back to the user
    :return:
    """
    if score >= 8:
        print("Severe: Mutation in highly conserved position. Score between 1-3")
    elif score >= 6:
        print("Potentially severe: Mutation in moderately conserved position. Score between 4-6")
    elif score >= 3:
        print("Unlikely severe: Mutation in mildly conserved position. Score between 7-8")
    elif 3 > score > 1.5:
        print("Inconsequential: Mutation in a not conserved position. Score around 9")
    else:
        print("Completely not important: Mutation has no effect. Score is 10")


def arg_parse():
    """
    This function creates an argparse parser and adds arguments.
    """

    # Create parser with a description and usage
    parser = argparse.ArgumentParser(
        description='Determines conservation of a site in where the snp was introduced',
        usage='python3.7 snp.py -n [SNP] -p [Gene position] -f [file]'
    )

    # Add arguments:
    parser.add_argument('-n', '--snp', type=str, metavar='', required=True,
                        help='SNP Nucleotide {A, C, T, G}')
    parser.add_argument('-p', '--pos', type=int, metavar='', required=True,
                        help='Gene position {Example: 21}')
    parser.add_argument('-m', '--msa', type=str, metavar='', required=True,
                        help='file with nucleotides {Example: gene.txt}')

    return parser.parse_args()


def main(args):
    """
    Main method with exceptions
    """

    try:
        aa_acid, new_aa = find_amino(args.pos, args.snp, args.msa)
        if aa_acid != new_aa:
            acid_position = math.ceil(args.pos / 3 - 1)
            scorings_matrix = mutation_effect(acid_position, new_aa)
            determine_severity(scorings_matrix)

    except IndexError:
        print("Oeps! Something went wrong, this index is out of range please try again")
    except KeyError:
        print("Oeps! Something went wrong, select between the following nucleotides {A, C, T, G}")
    except FileNotFoundError:
        print("Oeps! Something went wrong, This file doesnt exist here...")
    return 0


if __name__ == "__main__":
    sys.exit(main(arg_parse()))
