READ_ME: snp.py

author: Lisan Eisinga
date: 01-11-2021
files: snp.py, MSA.clustal, gene.txt

Description of the code:
	The script snp.py will handle the following input arguments, please use these correctly

    -n: This is the nucleotide change select {A, C, T, G}
    -p: The position in the sequence where the SNP needs to be implemented and scored
    -s: The sequence file.
	The program snp_annotation.py will handle a few input arguments:

When provided with all the correctly formatted arguments the program will start off with the usage of the gene.fasta file
and the MSA file to align these with eachother. After this the SNP will be implemented in the given position, please note
that this does have to be a valid position otherwise an exception will occur.
After everything is inputted correctly the script will give the SNP a score ten being the mutation is not of import at all
and one being a mutation in a highly conserved area. The script will write these results to the terminal.
Also note that any wrong file, position and nucleotide will result in an exception.

  How to interpret the score:
    * 1 - 3 score:
        Highly conserved area! Mutation has potentially very bad effects
    
    * 4 - 6 score:
        This SNP may be in an conserved area bit it does not look very bad.

    * 7 - 9 score:
        This SNP is most likely not in an conserved area and therefore the mutation is not bad.

    * 10 score:
        This SNP has no effect what so ever.!

Usage:
	Open any terminal that can use Python3

	- python3 snp.py -n [SNP being an {A, C, T, G}] -p [Postion of the SNP] -s [Sequence file]

Example of proper usage:
	- python3 snp.py -n A -p 12 -s gene.txt

Contact:
  For any questions please contact the email adress below.
  j.l.a.eisinga@st.hanze.nl